package com.romu31.demo;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
 class DemoApplicationTests {

	@Test
	 void contextLoads() {
		DemoApplication.main(new String[] {});
		assertTrue(true);
	}

}
