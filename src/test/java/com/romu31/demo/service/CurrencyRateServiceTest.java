package com.romu31.demo.service;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CurrencyRateServiceTest {

	@Mock
	private RestTemplate rateRestTemplate;

	@InjectMocks
	private CurrencyRateService currencyRateService ;

	private static final double DELTA = 1e-15;

	@Test
	@DisplayName("Test ok")
	 void getRateTestOK()  {
		String ok = "{\"base\":\"CAD\",\"rates\":{\"USD\":0.7543534896},\"date\":\"2019-09-24\"}";
		ResponseEntity<String> response = new ResponseEntity<>( ok,HttpStatus.OK);

		Mockito.when(rateRestTemplate.getForEntity("https://api.ratesapi.io/api/latest?base=CAD&symbols=USD",String.class)).thenReturn(response);
		assertEquals(0.7543534896,currencyRateService.getRate("CAD","USD").doubleValue(), DELTA);

	}

	@Test
	@DisplayName("Test ko")
	 void getRateException(){
		String ko = null;
		ResponseEntity<String> response = new ResponseEntity<>( ko,HttpStatus.BAD_REQUEST);

		Mockito.when(rateRestTemplate.getForEntity("https://api.ratesapi.io/api/latest?base=CAD&symbols=USD",String.class)).thenReturn(response);
		assertThrows(RateException.class, () -> {
			currencyRateService.getRate("CAD","USD");
		});
	}


}
