package com.romu31.demo.config;

import com.romu31.demo.service.CurrencyRateService;
import com.romu31.demo.service.RateException;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class RestResponseEntityExceptionHandlerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CurrencyRateService currencyRateService ;

	@Test
	public void testHandleBusinessError() throws Exception {
		Mockito.when(currencyRateService.getRate("CAD","USD")).thenThrow(new RateException());
		mockMvc.perform(get("/get/100/CAD/USD")).andExpect(content().string(containsString("Error Business layer")));

	}

	@Test
	public void testHandleGenericError() throws Exception {
		Mockito.when(currencyRateService.getRate("CAD","USD")).thenThrow(new NullPointerException());
		mockMvc.perform(get("/get/100/CAD/USD")).andExpect(content().string(containsString("Error Generic")));

	}

}
