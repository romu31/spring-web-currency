package com.romu31.demo.web;


import com.romu31.demo.service.CurrencyRateService;
import com.romu31.demo.to.ResponseTO;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CurrencyControllerTest {

	@LocalServerPort
	private int port;

	@InjectMocks
	private CurrencyController currencyController;

	@Mock
	private CurrencyRateService currencyRateService ;

	@Test
	public void getBaseTargetStatus200() throws Exception {
		Mockito.when(currencyRateService.getRate("CAD","USD")).thenReturn(0.7543534896);
		ResponseEntity<ResponseTO> res = currencyController.getBaseTarget(150.0,"CAD","USD");
				//113.15302344000001
		assertEquals(HttpStatus.OK, res.getStatusCode());

	}

	@Test
	public void getBaseTargetOK() throws Exception {
		Mockito.when(currencyRateService.getRate("CAD","USD")).thenReturn(0.7543534896);
		ResponseEntity<ResponseTO> res = currencyController.getBaseTarget(150.0,"CAD","USD");
		String expected = String.format("amount %s in %s  is %s in %s", 150.0, "CAD", 113.15302344000001, "USD");
		assertEquals(expected, res.getBody().getLabel());
	}

}
