package com.romu31.demo.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {

	@Bean
	public RestTemplate rateRestTemplate(){
		return new RestTemplate();
	}
	// https://api.ratesapi.io/api/latest?base=CAD&symbols=USD
	//https://api.ratesapi.io/api/latest

}
