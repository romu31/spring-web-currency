package com.romu31.demo.config;

import com.romu31.demo.service.RateException;
import com.romu31.demo.to.BusinessErrorResponse;
import com.romu31.demo.to.GenrericErrorResponse;
import com.romu31.demo.to.ResponseTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class RestResponseEntityExceptionHandler
		extends ResponseEntityExceptionHandler {


	@ExceptionHandler(value
			= { RateException.class })
	public ResponseEntity<? extends ResponseTO> handleBusinessError(
			final RateException ex) {
		return generateResponse(BusinessErrorResponse.builder().errorLabel("Error Business layer").build(),HttpStatus.BAD_REQUEST);

	}


	@ExceptionHandler(value
			= {  RuntimeException.class })
	public ResponseEntity<? extends ResponseTO> handleGenericError(final RuntimeException ex, WebRequest request) {
		String bodyOfResponse = "Error Generic";
		return generateResponse(GenrericErrorResponse.builder().errorLabel(bodyOfResponse).build(),HttpStatus.BAD_REQUEST);
	}

	public static ResponseEntity<? extends ResponseTO> generateResponse ( ResponseTO c, HttpStatus status){
		return new ResponseEntity(c,
				new HttpHeaders(), status);
	}

}
