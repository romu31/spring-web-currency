package com.romu31.demo.web;

import com.romu31.demo.to.RateResult;
import com.romu31.demo.to.ResponseTO;
import com.romu31.demo.service.CurrencyRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyController {

	private CurrencyRateService currencyRateService;

	@Autowired
	public CurrencyController(CurrencyRateService currencyRateService) {
		this.currencyRateService = currencyRateService;
	}

	@GetMapping("/get/{amount}/{baseCurrency}/{targetCurrency}")
	public ResponseEntity<ResponseTO> getBaseTarget(@PathVariable Double amount, @PathVariable String baseCurrency, @PathVariable String targetCurrency) {
		Double rate = currencyRateService.getRate(baseCurrency, targetCurrency);
		ResponseTO res =  RateResult.builder().label(String.format("amount %s in %s  is %s in %s", amount, baseCurrency, amount * rate, targetCurrency)).build();
		return  new ResponseEntity<>(res, HttpStatus.OK);
	}

}
