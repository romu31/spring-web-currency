package com.romu31.demo.service;


import com.jayway.jsonpath.JsonPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CurrencyRateService {

	private RestTemplate rateRestTemplate;

	private static final String RATE_RESOURCE_URL
			= "https://api.ratesapi.io/api/latest?base=%s&symbols=%s";

	private static final String REGEX_RATE = "$.rates.%s" ;

	@Autowired
	public CurrencyRateService(RestTemplate rateRestTemplate) {
		this.rateRestTemplate = rateRestTemplate;
	}

	public Double getRate(String baseCurrency, String targetCurrency)  {

		String url = String.format(RATE_RESOURCE_URL, baseCurrency, targetCurrency);
		try {
			ResponseEntity<String> response
					= rateRestTemplate.getForEntity(url, String.class);
			return JsonPath.parse(response.getBody()).read(String.format(REGEX_RATE,targetCurrency), Double.class);
		} catch (Exception ex) {
			throw new RateException();
		}
	}


}
