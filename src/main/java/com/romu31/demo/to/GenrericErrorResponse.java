package com.romu31.demo.to;

import lombok.Builder;

public class GenrericErrorResponse extends ResponseTO {

	@Builder
	public GenrericErrorResponse(String errorLabel) {
		super("",errorLabel);
	}

}
