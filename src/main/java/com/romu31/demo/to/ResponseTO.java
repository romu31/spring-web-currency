package com.romu31.demo.to;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public  class ResponseTO {

	private String label;

	private String errorLabel;

}
