package com.romu31.demo.to;

import lombok.Builder;

public class BusinessErrorResponse extends ResponseTO {

	@Builder
	public BusinessErrorResponse(String errorLabel) {
		super("",errorLabel);
	}

}
