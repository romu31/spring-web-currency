package com.romu31.demo.to;

import lombok.Builder;


public class RateResult extends ResponseTO {

	@Builder
	public RateResult(String label) {
		super(label,"");
	}
}
